var express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser'),
    path = require('path'),
    methodOverride = require('method-override');

//getting routes
var timeSheetRoutes = require("./routes/timeSheet"),
    salaryRoutes  = require('./routes/salaries');

mongoose.connect("mongodb://maria:tree@ds139428.mlab.com:39428/wagecalculator");

app.set("view engine","ejs");
app.set('views', path.join(__dirname, 'views'));
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));

//using routes
app.use(timeSheetRoutes);
app.use(salaryRoutes);

// ==============
//    ROUTES
// ==============
//INDEX ROUTE
app.get("/",function(req, res){
    res.render("home");
});

//listening for port
app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Server has started!");
});