## Wage Calculator *by Maria Syed*

Application to calculate employee salaries for the month using data from a csv file. Entries can also be added, uploaded and deleted.  
**Can be used at:** https://morning-eyrie-64726.herokuapp.com/

## What Was Used

* **MEN Stack**
    * MongoDB to store data
    * Express framework using ejs to create routes
    * Nodejs to use data 
* **Libraries**
    * mongoose to connect to mongoDB
    * momentjs to handle time
    * fast-csv to get data from csv file
    * method-override to perform put and delete requests
    * Bootstrap for styling
* Application deployed on heroku
* Programmed through cloud9

## How it works

* Data is read from "HourList201403.csv" and is used to populate database on mongoDB.     

* As data is being read, daily wage for each entry is calculated and added as a new field to each entry which can be viewed at https://morning-eyrie-64726.herokuapp.com/time-sheet

* New entries can be added to collection by filling in the form on the page.  
(*Note: data must be of certain format to be added successfully*)

* Entries can be editted or deleted from the table by clicking on the icons next to each table data.

* Collection can also be refreshed by clicking on the refresh button (top right). This means all data from the time sheet is removed and data is read from "HourList201403.csv" again to re-populate collection. All new entries or changes will be
 lost.

* Clicking on "Calculate Salary" prompts the database to group the entries according to their id and summing their daily wages to get salary for the month.

### How Daily Wages are Calculated

* For hours between 6:00 and 18:00, employees get paid $3.75/hour
* For hours between 18:00 and 6:00 employees get paid +$1.15/hour
* If hours worked are greater than 8,
    * For first 2 hours, add 3.75 * 0.25 
    * For next 2 hours, add 3.75 + 0.50
    * For rest hours, add 3.75 * 1.00 
* All is added to result in daily wage

