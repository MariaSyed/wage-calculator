var mongoose = require('mongoose');

var employeeSchema = new mongoose.Schema({
    name: String,
    id: String,
    date: String,
    startTime: String,
    endTime: String,
    dailyWage: Number
});

module.exports = mongoose.model("Employee", employeeSchema);