var express = require('express'),
    router = express.Router(),
    Employee = require('../models/employee'),
    Salary = require('../models/salary');
    
//show salaries
router.get("/salaries", function(req, res){
    Salary.find({},function(err, employees){
        if (err){
            console.log(err);
        } else {
            console.log(employees);
            res.render("result",{rEmployees: employees});
        }
    });
});

//process salaries
router.post("/get-salaries", function(req, res){
    Employee.aggregate( [
      { $group : { _id : "$id", id : { $first: "$id" },name : { $first: "$name" }, salary: { $sum: "$dailyWage" } } },
      { $out : "salaries" }], function(err){
          if(err){
              console.log(err);
          } else {
              console.log("Aggregated!");
              res.redirect("/salaries");
          }
      });
});

module.exports = router;