var express = require('express'),
    router = express.Router(),
    Employee = require('../models/employee'),
    calculate = require('../calculate'),
    refreshDB = require('../refresh.js');

//GET DATA
router.get("/time-sheet", function(req, res){
    Employee.find({},function(err, employees){
        if (err) {
            console.log(err);
        } else {
            //calculate.calculateDailyWage(employees);
            res.render("timeSheet", {employees: employees});
        }
    });
});



//REFRESH DATA
router.post("/time-sheet/refresh", function(req, res){
    //Removing all data and re-populating database from csv file
   refreshDB("HourList201403.csv");
   res.redirect("/time-sheet");
});

//ADD DATA
router.post("/time-sheet/add",function(req, res){
    var newEmployee = {
        name: req.body.name,
        id: req.body.id,
        date: req.body.date,
        startTime: req.body.startTime,
        endTime: req.body.endTime
    };
    //Calculating daily wage for employee created and storing it as dailyWage
    newEmployee.dailyWage = calculate.calculateDailyWage(newEmployee);
    Employee.create(newEmployee, function(err, employee){
        if(err){
            console.log(err);
        } else {
            console.log("Successfully added new employee!");
            res.redirect("/time-sheet");
        }
    });
});

//EDIT DATA
router.get("/time-sheet/:id/edit", function(req, res){
    Employee.findById(req.params.id, function(err, foundEmployee){
        if(err){
            res.redirect("/time-sheet");
        } else {
            res.render("edit", {employee: foundEmployee});
        }
    });
});

//UPDATE DATA
router.put("/time-sheet/:id", function(req, res){
    //Calculating daily wage for updated entry
    req.body.employee.dailyWage = calculate.calculateDailyWage(req.body.employee);
    Employee.findByIdAndUpdate(req.params.id, req.body.employee,function(err, updatedEmployee){
        if(err){
            res.redirect("/time-sheet");
        } else {
            res.redirect("/time-sheet");
        }
    });
});

//DELETE DATA
router.delete("/time-sheet/:id", function(req, res){
    Employee.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.redirect("/time-sheet");
        } else {
            console.log("deleted");
            res.redirect("/time-sheet");
        }
    });
});

module.exports = router;