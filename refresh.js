var Employee = require('./models/employee'),
    Salary = require('./models/salary'),
    calculate = require('./calculate'),
    csv = require('fast-csv'),
    fs = require('fs');

//read from csv file and populate collection with data
function refreshDB(filePath) {
    
    //removing all salaries
    Salary.remove({}, function(err){
        if(err){
            console.log(err);
        } else{
            console.log("Removed salaries!");
        }
    });
    //removing all employees
    Employee.remove({}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("Removed employee entries!");
            var stream = fs.createReadStream(filePath);
            
            //reading from file and for each row creating new employee variable
            csv.fromStream(stream, {headers : true}).on("data", function(data){
                var newEmployee = {
                    name: data['Person Name'],
                    id: data['Person ID'],
                    date: data['Date'],
                    startTime: data['Start'],
                    endTime: data['End']
                };
                
                //calculating daily wage for each employee and setting dailyWage field
                newEmployee.dailyWage = calculate.calculateDailyWage(newEmployee);
                //adding new entry in Employee collection using newEmployee
                Employee.create(newEmployee, function(err, employee){
                    if(err){
                        console.log(err);
                        console.log("Added new employee entry!");
                    } 
                });
            });
        }
    });
}


//exporting module as refreshDB to be used
module.exports = refreshDB;